#include <stdio.h>

struct student{
  char name[50];
  char subject[40];
  int marks;
};
void adddata(struct student *s1);
void printdata(struct student *s1);
int main(void) {

  int size;

  printf("Enter the number of students in class :");
  scanf("%d",&size);

  struct student studentdet[size];
  
  for(int i =0; i <size;i++){
    adddata(&studentdet[i]);
  }

  for(int i =0; i <size;i++){
    printdata(&studentdet[i]);
  }
  return 0;
}

void adddata(struct student *s1){
  printf("Enter student name : ");
  scanf("%s",s1->name);
  printf("Enter subject name : ");
  scanf("%s",s1->subject);
  printf("Enter student marks : ");
  scanf("%d",&s1->marks);
}

void printdata(struct student *s1){
  printf("\n------------------------------------");
    printf("\nThe student details of %s are ",s1->name);
    printf("\nsubject : %10s  |  marks: %10d ",s1->subject,s1->marks);
}
